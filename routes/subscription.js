const express = require('express'),
        router = express.Router(),
        {check, param, query} = require('express-validator'),
        auth = require('../controllers/authController'),
        userschema = require('../models/user'),
        serviceschema = require('../models/service'),
        subscription = require('../controllers/subscription'),
        { doc_upload, certificate_upload} = require('../services/upload.js'); 

router.post('/search', [
        check('query').notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], subscription.searchSubscription);

router.post('/advanced_search', auth.protect, [
        check('query').notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], subscription.advanceSubscriptionSearch);

router.post('/export', [
        check('fields').isString().withMessage('it must be a string').isLength({max:225}).withMessage('you have the maximum number of charcters')
], auth.protect, subscription.exportSubscription);

router.get('/', auth.protect, subscription.getAllSubscription);

//router.get('/testEmailAttachment', subscription.testEmailAttachment);

router.get('/company/:userId', [
        param('userId', 'input a valid mongo id').isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        query('sent', 'it must be a boolean').isBoolean().isIn(['true', 'false'])
], auth.protect, subscription.getSubscriptionsByCompany);

router.get('/:id', [
        param('id', 'input a valid mongo id').isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, subscription.getSubscription);

router.get('/download/subscription_document/:file_name', [
        param('file_name', 'please fill in the field name').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters').matches("^[a-zA-Z ,.'-]+$", "i").withMessage("The field name can only contain letters"),
], subscription.downloadSubscriptionDocument);

router.get('/download/certificate/:file_name', [
        param('file_name', 'please fill in the field name').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters').matches("^[a-zA-Z ,.'-]+$", "i").withMessage("The field name can only contain letters"),
], subscription.downloadCertificate);

router.post('/', doc_upload.array("documents", 20), [
        check('userID', 'User ID does not exist').notEmpty().isMongoId().custom(val => userschema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('serviceID', 'This service doesn\'t exist').notEmpty().isMongoId().custom(val => serviceschema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('subscriber_data', 'input your details for subscription').notEmpty().isJSON(),
        // check('duration', 'input the duration for your subscription').notEmpty().isInt().isIn([1,2,3,4]).withMessage('the options are 1 = yearly or 2 = bi-annual or 3 = quarterly or 4 =  monthly'),
        check('documents', 'upload your document').optional().notEmpty().isArray()
], auth.protect, subscription.createSubscription);

router.patch('/upload_certificate/:id', auth.protect, certificate_upload.single("certificateURL"), [
        param('id', 'input a valid mongo id').isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('_id', 'input a valid mongo id').notEmpty().isMongoId().withMessage('it must be a mongo id').isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
], subscription.uploadCertificate);

router.patch('/:id', doc_upload.array("documents", 20), [
        param('id', 'input a valid mongo id').isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('_id', 'input a valid mongo id').notEmpty().isMongoId().withMessage('it must be a mongo id').isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('userID', 'input a valid user id').optional().notEmpty().isMongoId().custom(val => userschema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('serviceID', 'This service doesn\'t exist').optional().notEmpty().isMongoId().custom(val => serviceschema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('subscriber_data', 'input your details for subscription').optional().notEmpty().isJSON(),
        check('documents', 'upload your document').optional().notEmpty().isArray()
], auth.protect, subscription.updateSubscription);

router.delete('/:id', [
        param('id', 'input a valid id').isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, subscription.deleteSubscription);

module.exports = router;