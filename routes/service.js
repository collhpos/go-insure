const express = require('express'),
        router = express.Router(),
        {check, query, param} = require('express-validator'),
        auth = require('../controllers/authController'),
        {permissionConstant} = require('../constants/index'),
        userschema = require('../models/user'),
        productschema = require('../models/products'),
        service = require('../controllers/service');

router.post('/search', [
        check('query').notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], service.searchService);

router.post('/advanced_search', auth.protect, [
        check('query').notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], service.advanceServiceSearch);

router.get('/', service.getAllServices);

router.post('/export', [
        check('fields').isString().withMessage('it must be a string').isLength({max:225}).withMessage('you have the maximum number of charcters')
], auth.protect, service.exportServices);

router.get('/:id', [
        param('id', 'input a valid id').notEmpty().isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], service.getService);

router.post('/', [
        check('productID', 'This product doesn\'t exist').notEmpty().exists().isMongoId().custom(val => productschema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('userID', 'This user doesn\'t exist').notEmpty().exists().isMongoId().custom(val => userschema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('requirement', 'fill in the requirement').notEmpty(),
        check('summary', 'give a summary').notEmpty(),
        check('description', 'give a description').notEmpty(),
        check('valueType', 'The options are flat rate = 2 or percentage = 1').notEmpty().isNumeric().isIn([1, 2]).isLength({min: 1, max: 2}).withMessage('You have exceeded the maximum number of characters'),
        check('valueAmount').notEmpty().isNumeric().isDecimal(),
        check('duration', 'The options are 1 = yearly or 2 = bi-annual or 3 = quarterly or 4 =  monthly or 5 =  Flat').notEmpty().isInt().isIn([1, 2, 3, 4, 5]).isLength({min: 1, max: 5}).withMessage('You have exceeded the maximum number of characters'),
], auth.protect, auth.restrictTo(permissionConstant.SUPERADMIN, permissionConstant.ADMIN), service.createService);

router.patch('/:id', [
        check('productID', 'This product doesn\'t exist').optional().notEmpty().isMongoId().custom(val => productschema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('userID', 'This user doesn\'t exist').optional().notEmpty().isMongoId().custom(val => userschema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('requirement', 'fill in the requirement').optional().notEmpty(),
        check('summary', 'give a summary').optional().notEmpty(),
        check('description', 'give a description').optional().notEmpty(),
        check('valueType', 'The options are flat rate = 2 or percentage = 1').optional().notEmpty().isNumeric().isIn([1, 2]).isLength({min: 1, max: 2}).withMessage('You have exceeded the maximum number of characters'),
        check('valueAmount').optional().notEmpty().isNumeric().isDecimal(),
        check('duration', 'The options are 1 = yearly or 2 = bi-annual or 3 = quarterly or 4 =  monthly or 5 =  Flat').optional().notEmpty().isInt().isIn([1, 2, 3, 4, 5]).isLength({min: 1, max: 5}).withMessage('You have exceeded the maximum number of characters'),
], auth.protect, auth.restrictTo(permissionConstant.SUPERADMIN, permissionConstant.ADMIN), service.updateService);

router.delete('/:id', auth.protect, [
        param('id', 'input a valid id').notEmpty().isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], service.deleteService);

module.exports = router;