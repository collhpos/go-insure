const express = require('express'),
        router = express.Router(),
        {check, param} = require('express-validator'),
        auth = require('../controllers/authController'),
        bank = require('../controllers/bank');

router.post('/createDetails', [
  check('name', 'please enter your bank name').notEmpty().matches("^[a-zA-Z ()]+$", "i").withMessage('it must be alphabets').isLength({max: 50}).withMessage('you have exceeded the maximum number of characters'),
  check('code', 'insert a valid bank code').notEmpty().isInt().withMessage('it must be an integer').isLength({max:10}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, bank.createBankAccount);

router.post('/search', [
  check('query').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
  check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
  check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, bank.searchAccount);

router.post('/advance_search', [
  check('query').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
  check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
  check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, bank.adSearchAccount);

router.get('/allAccounts', bank.getAllAccounts);

router.get('/account/:id', [
  param('id', 'input a valid mongo id').isMongoId().isLength({max:25}).withMessage('you have exceeded the maximum number of characters')
], bank.getAccountDetail);

router.patch('/update/:id',[
  param('id', 'input a valid mongo id').isMongoId().isLength({max:25}).withMessage('you have exceeded the maximum number of characters'),
  check('_id', 'input a valid mongo id').isMongoId().isLength({max:25}).withMessage('you have exceeded the maximum number of characters'),
  check('name', 'please enter your bank name').optional().notEmpty().matches("^[a-zA-Z ()]+$", "i").withMessage('it must be alphabets').isLength({max: 50}).withMessage('you have exceeded the maximum number of characters'),
  check('code', 'insert a valid bank code').optional().notEmpty().isInt().withMessage('it must be an integer').isLength({max:10}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, bank.updateAccount);

router.delete('/delete/:id', [
  param('id', 'input a valid mongo id').isMongoId().isLength({max:25}).withMessage('you have exceeded the maximum number of characters')
], auth.protect, bank.deleteAccount);

module.exports = router;