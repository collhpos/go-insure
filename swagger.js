const {postCompany, registerCompanyAdmin,changePassword, getUserDocuments, getUserDocument, advancedUserSearch, getAccountName, exportUser, transfer, deleteUploadedFile, getUsers, searchUser, confirmAccount, forgotPassword, resetPassword, updateUser, addBankDetails, documentUpload, kycFormUpload, updateCompany, getUser, deleteUser, loginUser, logoutUser, postAdmin, postCustomer} = require('./openApi/user.swagger'),
    {getPayments, getPayment, postPayment, exportPayment, getPaymentsByCompany, advancedPaymentSearch, searchPayment, updatePayment, deletePayment} = require('./openApi/payment.swagger'),
    {getProducts, getProduct, postProduct, exportProduct, searchProduct, advancedProductSearch, updateProduct, deleteProduct} = require('./openApi/product.swagger'),
    {getServices, getService, postService, exportService, searchService, advancedServiceSearch, updateService, deleteService} = require('./openApi/service.swagger'),
    {getSubscriptions, getSubscription, getSubscriptionsByCompany, downloadCertificate, advancedSubscriptionSearch, postSubscription, exportSubscription, downloadSubscriptionDocument, searchSubscription, updateSubscription, uploadCertificate, deleteSubscription} = require('./openApi/susbscription.swagger'),
    {countEntities} = require('./openApi/dashboardEntities.swagger'),
    {countCompanyEntities} = require('./openApi/companyEntities.swagger'),
    {addSettlement, getSettlementHistory, exportSettlementHistory, allSettlementHistory, updateSettlementHistory, deleteSettlementHistory, searchSettlementHistory} = require('./openApi/settlementHistory.swagger'),
    {createBankDetails, getBankDetails, getAllBanks, updateBank, deleteBank, searchBank, adSearchBank} = require('./openApi/bank.swagger'),
    {components} = require('./openApi/components.swagger');

module.exports = {
    openapi: '3.0.1',
    info: {
        version: '1.0.0',
        title: 'Go-Insure API Documetation',
        description: 'Go-Insure API Documetation',
        termsOfService: '',
        contact: {
            name: 'Gbenga Sodunke',
            email: 'gbengasodunke@zyonel.com',
            url: 'https://zyonel.com'
        },
        license: {
            name: 'Apache 2.0',
            url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
        }
    },
    servers: [
        {
            url: 'http://localhost:3001/api/v1/',
            description: 'Local server'
        },
        {
            url: 'https://go-insure-api.herokuapp.com/api/v1/',
            description: 'DEV Env'
        },
        {
            url: 'https://qa-goinsure-api.herokuapp.com/api/v1/',
            description: 'QA Env'
        },
        {
            url: 'http://staging-api.goinsure.ng/api/v1/',
            description: 'UAT Env'
        },
        {
            url: 'http://production-api.goinsure.ng/api/v1/',
            description: 'Live Server'
        }

    ],
    components: {
        schemas: {},
        securitySchemes: {
            bearerAuth: {
                type: 'http',
                scheme: 'bearer',
                bearerFormat: 'JWT'
            }
        }
    },
    tags: [
        {
            name: 'User'
        },
        {
            name: 'Subscription'
        },
        {
            name: 'Service'
        },
        {
            name: 'Product'
        },
        {
            name: 'Payment'
        },
        {
            name: 'DashboardEntities'
        },
        {
            name: 'CompanyEntities'
        },
        {
            name: 'settlementHistory'
        },
        {
            name: 'Bank'
        }
    ],
    paths: {
        "/bank/createDetails": {
            post: createBankDetails
        },
        "/bank/search": {
            post: searchBank
        },
        "/bank/advance_search": {
            post: adSearchBank
        },
        "/bank/allAccounts": {
            get: getAllBanks
        },
        "/bank/account/:id": {
            get: getBankDetails
        },
        "/bank/update/:id": {
            patch: updateBank
        },
        "/bank/delete/:id": {
            delete: deleteBank
        },
        "/settlementHistory/addSettlement": {
            post: addSettlement
        },
        "/settlementHistory/export": {
            post: exportSettlementHistory
        },
        "/settlementHistory/search": {
            post: searchSettlementHistory
        },
        "/settlementHistory/allSettlement": {
            get: allSettlementHistory
        },
        "/settlementHistory/:id": {
            get: getSettlementHistory
        },
        "/settlementHistory/update/:id": {
            patch: updateSettlementHistory
        },
        "/settlementHistory/delete/:id": {
            delete: deleteSettlementHistory
        },
        "/companyEntities/count": {
            get: countCompanyEntities
        },
        "/dashboardEntities/count": {
            get: countEntities
        },
        "/user": {
            get: getUsers
        },
        "/user/customer/signup": {
            post: postCustomer         
        },
        "/user/company/signup": {
            post: postCompany          
        },
        "/user/company/register_admin": {
            post: registerCompanyAdmin
        },
        "/user/admin/signup": {
            post: postAdmin          
        },
        "/user/search": {
            post: searchUser
        },
        "/user/advanced_search": {
            post: advancedUserSearch
        },
        "/user/getAccountName": {
            post: getAccountName
        },
        "/user/transfer": {
            post: transfer
        },
        "/user/export": {
            post: exportUser
        },
        "user/confirm_account/{random_character}":{
            patch: confirmAccount
        },
        "/user/forgot_password/{email}": {
            get: forgotPassword
        },
        "/user/change_password/{id}": {
            patch: changePassword
        },
        "/user/reset_password/{random_character}": {
            patch: resetPassword
        },
        "/user/documentUpload/:id":{
            patch: documentUpload
        },
        "/user/kyc_form/:id": {
            patch: kycFormUpload
        },
        "/user/{id}": {
            patch: updateUser,
            get: getUser,
            delete: deleteUser
        },
        "/user/{id}/documents": {
            get: getUserDocuments,            
        },
        "/user/document/{id}": {
            get: getUserDocument,
        },
        "/user/company/{id}": {
            patch: updateCompany,
        },
        "/user/bankDetails/{id}": {
            patch: addBankDetails
        },
        "/user/login": {
          post: loginUser
        },
        "/user/logout": {
          get: logoutUser
        },
        "/user/delete_file/{user_id}": {
            patch: deleteUploadedFile
        },
        "/payment": {
            get: getPayments,
            post: postPayment
        },
        "/payment/search": {
            post: searchPayment
        },
        "/payment/advanced_search": {
            post: advancedPaymentSearch
        },
        "/payment/export": {
            post: exportPayment
        },
        "/payment/{id}": {
          patch: updatePayment,
          get: getPayment,
          delete: deletePayment
        },
        "/payment/company/{userId}": {
            get: getPaymentsByCompany,
        },
        "/product/": {
            get: getProducts,
            post: postProduct
        },
        "/product/search": {
            post: searchProduct
        },
        "/product/advanced_search": {
            post: advancedProductSearch
        },
        "/product/export": {
            post: exportProduct
        },
        "/product/{id}": {
          patch: updateProduct,
          get: getProduct,
          delete: deleteProduct
        },
        "/service": {
            get: getServices,
            post: postService
        },
        "/service/search": {
            post: searchService
        },
        "/service/advanced_search": {
            post: advancedServiceSearch
        },
        "/service/export": {
            post: exportService
        },
        "/service/{id}": {
          patch: updateService,
          get: getService,
          delete: deleteService
        },
        "/subscription": {
            get: getSubscriptions,
            post: postSubscription 
        },
        "/subscription/search": {
            post: searchSubscription
        },
        "/subscription/advanced_search": {
            post: advancedSubscriptionSearch
        },
        "/subscription/export": {
            post: exportSubscription
        },
        "/subscription/company/{userId}": {
            get: getSubscriptionsByCompany,
        },
        "/subscription/{id}": {
          patch: updateSubscription,
          get: getSubscription,
          delete: deleteSubscription
        },
        "/subscription/upload_certificate/:id": {
          patch: uploadCertificate
        },
        "/subscription/download/subscription_document/:key": {
            get: downloadSubscriptionDocument
        },
        "/subscription/download/certificate/:key": {
            get: downloadCertificate
          }
    },
    components
  }
