
exports.permissionConstant = {
    SUPERADMIN : 1,
    ADMIN: 2
}

exports.durationConstant = {
    YEARLY : 1,
    BIANNUAL: 2,
    QUARTERLY : 3,
    MONTHLY: 4,
}

exports.roleConstant = {
    ADMIN  : 1,
    COMPANY : 2,
    CUSTOMER : 3
}