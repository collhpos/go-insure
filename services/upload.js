require('dotenv').config();
const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const AppError = require('../utils/appError');
const path = require('path');
const {fs} = require('memfs');

const s3Config = new AWS.S3({
    accessKeyId: process.env.S3_ID,
    secretAccessKey: process.env.S3_SECRET,
    bucket: process.env.S3_BUCKET
  });


///////////////////////////////////////////////////////////////////
////// Image Uploader Begins
///////////////////////////////////////////////////////////////////

const imageFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/gif') {
        cb(null, true)
    } else {
        console.log("got here!")
        return cb(new AppError(400, 'Only image files of type jpg, png and gif are allowed are allowed!'), false);
    }

    if(file.size && file.size > 1024 * 1024 * 0.2) {
        return cb(new AppError(400, 'File size too large. FIle size should not exceed 200kb'), false);
    }
    else {
        cb(null, true)
    }   
}

const logoS3Config = multerS3({
    s3: s3Config,
    bucket: `${process.env.S3_BUCKET}/logos`,
    metadata: function (req, file, cb) {
        cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
        cb(null, `${file.fieldname}_${+new Date()}${path.extname(file.originalname).toLowerCase()}`)
    }
});

const logo_upload = multer({
    storage: logoS3Config,
    fileFilter: imageFilter,
    limits: {
        fileSize: 1024 * 1024 * 0.5 // we are allowing only 5 KB files
    }
})
///////////////////////////////////////////////////////////////////
////// Image Uploader Ends
///////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////
////// Document Uploader Begins
///////////////////////////////////////////////////////////////////

const fileFilter = (req, file, cb) => {
    if (!file.originalname.match(/\.(png|jpeg|jpg|gif|pdf|doc|docx|rtf)$/)) {
        return cb(new AppError(400, 'Only image files of type pdf, doc, docx, jpeg, jpg, gif and rtf are allowed are allowed!'), false);
    } else {
        cb(null, true);
    }
    console.log(file)
    if(file.size && file.size > 1024 * 1024 * 5) {
        return cb(new AppError(400, 'File size too large. FIle size should not exceed 5MB'), false);
    }
    else {
        cb(null, true)
    }   
}

const fileS3Config = multerS3({
    s3: s3Config,
    bucket: `${process.env.S3_BUCKET}/documents`,
    metadata: function (req, file, cb) {
        cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
        const key = `${file.fieldname}_${+new Date()}${path.extname(file.originalname).toLowerCase()}`;
        file.sfile_name = key;

        cb(null, key)
    }
});

const doc_upload = multer({
    storage: fileS3Config,
    fileFilter: fileFilter,
    limits: {
        fileSize: 1024 * 1024 * 1 // we are allowing only 1MB files
    }
})

///////////////////////////////////////////////////////////////////
////// Document Uploader Ends
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
////// Document Uploader Begins
///////////////////////////////////////////////////////////////////

const certifiateS3Config = multerS3({
    s3: s3Config,
    bucket: `${process.env.S3_BUCKET}/certificates`,
    metadata: function (req, file, cb) {
        cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
        cb(null, `${file.fieldname}_${+new Date()}${path.extname(file.originalname).toLowerCase()}`)
    }
});

const certificate_upload = multer({
    storage: certifiateS3Config,
    fileFilter: fileFilter,
    limits: {
        fileSize: 1024 * 1024 * 2 // we are allowing only 2MB files
    }
})

///////////////////////////////////////////////////////////////////
////// Document Uploader Ends
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
////// Delete File Begins
///////////////////////////////////////////////////////////////////

const delete_file = async(file_path) => {
    s3Config.deleteObject({
        Bucket: process.env.S3_BUCKET,
        Key: file_path
      },function (err,data){
          console.log(err);
      })
}

///////////////////////////////////////////////////////////////////
////// Delete FIle Ends
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
////// Download File Begins
///////////////////////////////////////////////////////////////////

/* const download_file = async(file_path) => {
    
    const result = await s3Config.getObject({Bucket: process.env.S3_BUCKET, Key: "documents/"+file_path}).promise();
    
    return result;

} */

const download_file = async(file_path) => {
    
    //var fileStream = fs.createWriteStream('./downloads');
    var s3Stream = await s3Config.getObject({Bucket: process.env.S3_BUCKET, Key: file_path}).createReadStream();
    
    // Listen for errors returned by the service
    s3Stream.on('error', function(err) {
        // NoSuchKey: The specified key does not exist
        console.error(err);
    });
    
    return s3Stream;

}  

///////////////////////////////////////////////////////////////////
////// Download FIle Ends
///////////////////////////////////////////////////////////////////



exports.logo_upload = logo_upload; 
exports.doc_upload = doc_upload;
exports.certificate_upload = certificate_upload;
exports.delete_file = delete_file;
exports.download_file = download_file;