const mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate-v2');
    const decoder = require('html-entity-decoder');


//mongoose.ObjectId.get(v => v.toString());

const serviceSchema = new mongoose.Schema({
    productID: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
        ref: 'Products'
    },
    product_name: {
        type: String,
        require: true
    },
    parentID: {
        type: mongoose.Schema.Types.ObjectId,
        require: false,
        ref: 'Products'
    },
    userID: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
        ref: 'User'
    },
    subAccountID: {
        type: String
    },
    requirement: {
        type: mongoose.Schema.Types.Mixed,
        require: true,
    },
    summary: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true,
        get: decode,
    },
    valueType: {
        type: Number,
        min: 1,
        max: 2,
        default: 2,
        description: '1 = Percentage, 2 = Flat rate',
        require: true
    },
    valueAmount: {
        type: mongoose.Types.Decimal128,
        require: true
    },
    duration: {
        type: Number,
        require: true,
        min: 1,
        max: 5,
        default: 2,
        description: '1 = Yearly, 2 = Monthly, 3 = Quarterly, 4 = Bi-Annually, 5 = Flat'
    },
    companyRate: {
        type: mongoose.Types.Decimal128,
        require: true
    },
    published: {
        type: Boolean,
        require: true,
        default: false
    },

},{
    timestamps: { createdAt: true, updatedAt: false }
},{
    collation: { locale: 'en', strength: 2 }
 })

serviceSchema.set('toObject', { getters: true });
serviceSchema.set('toJSON', { getters: true });

 function decode(description) {
    if(description) {
        return decoder.feed(description);
    }

    return description;
  }

serviceSchema.statics = {
    isValid(id) {
       return this.findById(id)
            .then(result => {
                if (!result) throw new Error('Service not found')
            })
    },
    checkServiceExists(userID, productID) {
        return this.find({userID, productID})
            .then(result => {
                if (result) throw new Error('Service Already Exists!')
            })
    },
    isCompanyOwned(id, userID) {
        return this.find({id, userID})
            .then(result => {
                if (result) throw new Error('Service not owned by CompanyID supplied')
            })
    }
}

serviceSchema.plugin(mongoosePaginate);
const Service = mongoose.model('Service', serviceSchema);
module.exports = Service;