const mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate-v2');

const paymentSchema = new mongoose.Schema({
    userID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        require: true
    },
    user_name: {
        type: String,
        require: true
    },
    subscriptionID : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Subscription',
        require: true
    },
    serviceID : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Service',
        require: true
    },
    amount: {
        type: mongoose.Types.Decimal128,
        require: true
    },
    authorizationCode: {
        type: String,
        require: false
    },
    isSettled: {
        type: Boolean,
        default: false
    }
},{
    timestamps: { createdAt: true, updatedAt: false }
},{
    collation: { locale: 'en', strength: 2 }
 })

paymentSchema.statics = {
    isValid(id) {
       return this.findById(id)
              .then(result => {
                 if (!result) throw new Error('payment id not found')
       })
    }
}

paymentSchema.plugin(mongoosePaginate);
const Payment = mongoose.model('Payment', paymentSchema);
module.exports = Payment;