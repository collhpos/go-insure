const mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate-v2');

const productsSchema = new mongoose.Schema({
    id: {
        type: Number,
        require: true
    },
    name: {
        type: String,
        require: true,
        index: true
    }, 
    slug: {
        type: String
    },   
    description: {
        type: String,
        require: true
    },
    parentID: {
        type: mongoose.Schema.Types.ObjectId,
        require: false,
        ref: 'Products' 
    },
    has_parent: Boolean,
    has_child: Boolean,
    requirement: mongoose.Schema.Types.Mixed,
    useParentRequirement: Boolean,
    isActive: {
        type: Boolean,
        require: true,
        default: true
    },
    published: {
        type: Boolean,
        require: true,
        default: true
    },
},{
    timestamps: { createdAt: true, updatedAt: false }
},{
    collation: { locale: 'en', strength: 2 }
 })

productsSchema.virtual('parent',{
   ref: 'Products',
   localField: '_id',
   foreignField: 'parentID',
   justOne: false,
},{ toJSON: { virtuals: true } }); /* toJSON option is set because virtual fields are not included in toJSON output by default. So, if you don't set this option, and call Product.find().populate('parent'), you won't get anything in parent */

productsSchema.statics = {
    isValid(id) {
       return this.findById(id)
              .then(result => {
                 if (!result) throw new Error('product not found')
       })
    }
}

productsSchema.plugin(mongoosePaginate);

const Products = mongoose.model('Products', productsSchema);
module.exports = Products;