const mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate-v2');

const userDocumentsSchema = new mongoose.Schema({
    userID : {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
        ref: 'User'
    },
    documents: [],   

},{
    timestamps: { createdAt: true, updatedAt: false },
},{
    collation: { locale: 'en', strength: 2 }
 });

userDocumentsSchema.plugin(mongoosePaginate);
const UserDocuments = mongoose.model('UserDocuments', userDocumentsSchema);
module.exports = UserDocuments;