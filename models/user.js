const mongoose = require('mongoose'),
    validator = require('validator'),
    bcrypt = require('bcryptjs'),
    mongoosePaginate = require('mongoose-paginate-v2'),
    utility = require('../services/utility');

const userSchema = new mongoose.Schema({
    firstname: {
        type: String,
        require: true
    },
    lastname: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true,
        index: true,
        unique: true,
        lowercase: true,
        sparse:true,
        validate: [validator.isEmail, ' Please provide a valid email']
    },
    password: {
        type: String,
        require: true,
        minLength: 6,
        select: false
    },
    remember_token: {
        type: String,
        require: false,
    },
    phone: {
        type: String,
        require: true
    },
    role: {
        type: Number,
        require: true,
        min: 1,
        max: 3,
        default: 3,
        description: '1 = admin, 2 = company, 3 = customer'
    },
    isActive: {
        type: Boolean,
        default: false,
        require: true
    },
    company_details: {
        company_name: {
            type: String,
        },
        company_address: {
            type: String,
        },
        company_email: {
            type: String,
            lowercase: true,
            unique:false,
            index: true,
            sparse: true,
            validate: [validator.isEmail, ' Please provide a valid email']
            },
        company_phone: {
            type: String,
        },
        logo_url: {
            type: String
        },
        kyc_form : {
            type: String
        },
        bank_details: {
            flutterSubAccount_id: {
                type: String
            },
            bank_id: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Bank'
            },
            account_name: {
                type: String
            },
            account_number: {
                type: String
            },
            bank_name: {
                type: String
            },
            bank_code: {
                type: String
            }
        }
    },
    confirmation_token: {
        type: String
    },
    remember_token: {
        type: String
    },
    parentID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    children: [{
        id: {
            type: mongoose.Schema.Types.ObjectId
        },
        firstname: {
            type: String,
            require: true
        },
        lastname: {
            type: String,
            require: true
        },
        email: {
            type: String,
            require: true,
            index: true,
            unique: true,
            lowercase: true,
            sparse:true,
            validate: [validator.isEmail, ' Please provide a valid email']
        },       
        phone: {
            type: String,
            require: true
        },        
        isActive: {
            type: Boolean,
            default: false,
            require: true
        }        
     }],
    documents:[],
    userType: {
        type: String,
        require: true
    },
    permission: []
},
{
    timestamps: { createdAt: true, updatedAt: false }
},{
    collation: { locale: 'en', strength: 2 }
 });

// encrypt the password using 'bcryptjs'
// Mongoose -> Document Middleware
userSchema.pre('save', async function hashPassword(next) {
  try {
    const user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    const salt = await bcrypt.genSalt(10);

    // hash the password along with our new salt
    const hash = await bcrypt.hash(user.password, salt);

    // override the cleartext password with the hashed one
    user.password = hash;
    return next();
  } catch (e) {
    return next(e);
  }
});


userSchema.statics = {
    isValid(id) {
       return this.findById(id)
              .then(result => {
                 if (!result) throw new Error('User not found')
       })
    },
    emailExists(email) {
        return this.findOne({email})
              .then(result => {
                 if (result) throw new Error('Email already exist')
       })
    },
    companyEmailExists(company_email) {
        return this.findOne({"company_details.company_email":  company_email})
              .then(result => {
                 if (result) throw new Error('Company email already exist')
       })
    },
    companyNameExists(company_name) {
        return this.findOne({"company_details.company_name":  company_name})
              .then(result => {
                 if (result) throw new Error('Company name already exist')
       })
    }
}

//
// This is Instance Method that is gonna be available on all documents in a certain collection
userSchema.methods.correctPassword = async function (typedPassword, originalPassword) {
    return await bcrypt.compare(typedPassword, originalPassword);
};

userSchema.plugin(mongoosePaginate);
const User = mongoose.model('User', userSchema);
module.exports = User;