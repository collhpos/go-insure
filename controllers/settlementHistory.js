const base = require('./baseController'),
        SettlementHistory = require('../models/settlementHistory');

exports.addSettlement = base.createOne(SettlementHistory);
exports.allSettlementHistory = base.getAll(SettlementHistory);
exports.getSettlementHistory = base.getOne(SettlementHistory);
exports.exportSettlementHistory = base.export(SettlementHistory);
exports.updateSettlementHistory = base.updateOne(SettlementHistory);
exports.searchSettlementHistory = base.search(SettlementHistory);
exports.deleteSettlementHistory = base.deleteOne(SettlementHistory);