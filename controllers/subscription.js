const Subscription = require('../models/subscription'),
        User = require('../models/user'),
        UserDocuments = require('../models/user_documents'),
        Service = require('../models/service'),
        Payment = require('../models/payment'),
        base = require('./baseController'),
        Email = require('../services/mail'),
        {durationConstant, roleConstant} = require('../constants/index'),
        {validationResult} = require('express-validator'),
        AppError = require('../utils/appError'),
        date = require('date-and-time'),
        { download_file } = require('../services/upload.js'); 


exports.createSubscription = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    const user = await User.findOne(req.userID); 
    if(!user) {
        return next(new AppError(402, 'fail', 'UserID does not exist'), req, res, next);
    }

    const service = await Service.findOne(req.serviceID).populate('productID', 'name');
    if(!service) {
        return next(new AppError(402, 'fail', 'serviceID does not exist'), req, res, next);
    }
    
    try {    
        const subscriber_data = transformSubscriberData(JSON.parse(req.body.subscriber_data));
        let documents = [];

        if(req.files && req.files.length > 0) {
             documents = transformDocumentsData(JSON.parse(req.body.documents_names), req.files);             
        }

        if(documents && documents.length > 0) {
            console.log("documents:",documents);
            await saveNewlyUploadedDocuments(req.userID, documents);
        }        

        req.body.subscriber_data =  subscriber_data;
        req.body.documents =  documents;
        req.body.user_name = user.firstname;
        req.body._user_email = user.email;

        const data = await Subscription.create(req.body);  
        if(!data){
          return next(new AppError(402, 'fail', 'Your subscription was not succesfull'), req, res, next);
        };  

        const product_name = service && service.productID && service.productID.name;
        console.log(req.files)
        const emailBody = '<p>Hello '+user.firstname+',</p><p> You have been successfully subscribed to the '+ product_name+' policy.</p>'; // HTML body
        const subject = `${product_name} Subscription Successful!`;
        
        // Mail User
        await Email.sendMail(user.email, subject, emailBody);

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

const transformSubscriberData = (subscribers_data) => {
    let data = {};

    subscribers_data && subscribers_data.map((item) => {
            for (let [key, value] of Object.entries(item)) {
            data[key]=value;            
        }
    });

    return data;
};

const transformDocumentsData = (documents_data, files) => {
    let data = [];
    console.log("documents_data", documents_data);

    files && files.map((file, index)  => { 
        console.log("documents_data_index", documents_data[index])
        file.field_name = documents_data[index].name;
        file.new = documents_data[index].new;
        file.file_name = documents_data[index].file_name;

        data.push(file);
    });
    
    return data;
};

const saveNewlyUploadedDocuments = async(userID, documents) => {

    try {
        const docs = documents && documents.filter(doc => doc.new === true);
        let user = null;

        if(docs && docs.length > 0) {
            user = await UserDocuments.findOneAndUpdate({userID}, 
                { $push: {documents: docs}}, {upsert:true, new: true, runValidators: true}
            )
        }  
        
        return user;
    }
    catch(error) {
        next(error);
    }
    
};

const populateOption = {
    populate:[{
        path:'userID',
        select: 'firstname lastname',
    },  
    {
        path: 'serviceID',
        select: 'productID userID valueType summary valueAmount',
        populate:[{
            path: 'productID',
            select: 'name'
        },
        {
            path: 'userID',
            select: 'company_details'
        }]
    }]
};

exports.getAllSubscription = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {

        var page = (req.query.page) ? req.query.page : 1;
        var perPage = (req.query.limit) ? req.query.limit :10;

        var options = {
            sort: req.query.sort || {createdAt: -1},  
            populate: populateOption.populate,            
            lean: true,
            page: page, 
            limit: perPage
        };

        const data = await Subscription.paginate({}, options);

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }

};

exports.downloadCertificate = async (req, res) => {

    try {
        const folder = 
        s3Stream = await download_file("certificates/"+req.params.file_name);

        res.status(200);
        res.setHeader("Accept-Ranges", "bytes");

        s3Stream.pipe(res).on('error', function(err) {
            // capture any errors that occur when writing data to the file
            console.error('File Stream:', err);
        }).on('close', function() {
            console.log('Done.');
        });

    } catch (error) {
        next(error);
    }       
    
}

exports.downloadSubscriptionDocument = async (req, res) => {

    try {        
        s3Stream = await download_file("documents/"+req.params.file_name);

        res.status(200);
        res.setHeader("Accept-Ranges", "bytes");

        s3Stream.pipe(res).on('error', function(err) {
            // capture any errors that occur when writing data to the file
            console.error('File Stream:', err);
        }).on('close', function() {
            console.log('Done.');
        });

    } catch (error) {
        next(error);
    }       
    
}

exports.getSubscription = base.getOne(Subscription);

exports.uploadCertificate = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
   
    try {        
        const mypath = req.file && req.file.location;
        const key = req.file && req.file.key;
        
        if(req.params.id !== req.body._id)  {
            return next(new AppError(404, 'fail', 'request id does not match the id supplied'), req, res, next);
        }

        const sub = await calculateSubscriptionPeriod(req.params.id);

        let subscription = await Subscription.
            findByIdAndUpdate(
                req.params.id, 
                {
                    $set: {
                        _id: req.body._id, 
                        certificateURL:mypath, 
                        key: key,
                        startDate:sub.startDate,
                        endDate:sub.endDate, 
                        certificateSent:true, 
                        certificateSentDate:sub.sentDate, 
                        isActive:true
                    }
                },  
            {
                new: true,
                runValidators: true,
            }).populate({
                path: 'serviceID',
                select: 'productID',
                populate:{
                    path: 'productID',
                    select: 'name'
                }
            });
            
        if (!subscription) {
          return next(new AppError(402, 'fail', 'No subscription document found with that id'), req, res, next);
        };

        const user = await User.findOne(subscription.userID);

        if (!user) {
          return next(new AppError(401, 'fail', 'No user document found with that id'), req, res, next);
        }
        
        const product_name = subscription && subscription.serviceID && subscription.serviceID.productID && subscription.serviceID.productID.name;
        const emailBody ='<p>Hello '+user.firstname+',</p><p> Please, click on the link below to download your insurance certificate you recently subscribed to on the Go-Insure platform.</p><p><a href="'+mypath+'"> Download Insurance Certificate</a> </p> <p>Regards.</p><p>Go Insure Team</p>'; // HTML body
        const subject = `${product_name} Certificate`;
        
        // Mail User
        await Email.sendMail(user.email, subject, emailBody);    

        res.status(200).json({
            status: 'success',
            subscription
        });
    } catch (error) {
        next(error);
    }
};

const calculateSubscriptionPeriod = async (id) => {
    const subscription = await Subscription.findById(id);
    const now = new Date();
    let sub = {};

    sub.startDate = now;
    sub.sentDate = now;
    sub.endDate = date.addDays(now, subscription.period);

    return sub;
};

/* exports.testEmailAttachment = async (req, res, next) => {

    try {
        await Email.sendMail('geebengs@yahoo.com', 'Test Email', 'This is to test email attachment');   

        res.status(200).json({
            status: 'success',
        });
    }
    catch(error) {
        next(error);
    }
} */

exports.getSubscriptionsByCompany = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };

    const user = await User.findById(req.params.userId);

    if(!user) {
      return next(new AppError(404, 'fail', 'No company document found with that id'), req, res, next);
    }
   
    Subscription.find({certificateSent: req.query.sent}).populate([{
        path: 'serviceID',
        select: 'productID userID valueType valueAmount',
        populate:{
            path: 'productID',
            select: 'name'
        }
    },{
        path: 'userID',
        select: 'firstname lastname email',
    }]).exec(function(err, subscriptions) {
        if(err) {
            next(error);
        }
        else {            
            result = subscriptions.filter(subscription => {                
                if(subscription && subscription.serviceID) {
                  return subscription.serviceID.userID == req.params.userId;
                }
                
            });     
            res.status(200).json({
                status: 'success',
                result
            });
        }       
    });
};

exports.deleteSubscription = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        const data = await Subscription.findByIdAndDelete(req.params.id);
        const subscriptionID = req.params.id;

        if (!data) {
          return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }

        base.deleteFromRelation(Payment, {subscriptionID});

        res.status(200).json({
            status: 'This document has been deleted'
        });
    } catch (error) {
        next(error);
    }
};

exports.exportSubscription = base.export(Subscription);
exports.updateSubscription = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {

        if(req.params.id !== req.body._id) {
            return next(new AppError(404, 'fail', 'The id provided doesn\'t match the user id you are trying to access'), req, res, next);
        }; 
        
        let documents = [];
        let subscriber_data = ""

        if(req.files && req.files.length > 0) {
            documents = transformDocumentsData(JSON.parse(req.body.documents_names), req.files);             
        };

        if(documents && documents.length > 0) {
            console.log("documents:",documents);
            await saveNewlyUploadedDocuments(req.userID, documents);
            req.body.documents = documents;
        };
        
        if(req.body.subscriber_data) {
            subscriber_data = transformSubscriberData(JSON.parse(req.body.subscriber_data));
            req.body.subscriber_data = subscriber_data;
        }

        const data = await Subscription.findByIdAndUpdate(req.params.id, {$set: req.body}, {
            new: true,
            runValidators: true,
        });
        if (!data) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        };

        res.status(200).json({
            status: 'updated',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.searchSubscription = base.search(Subscription, populateOption);
exports.advanceSubscriptionSearch = base.advance_search(Subscription, populateOption);