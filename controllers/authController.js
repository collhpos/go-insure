const {promisify} = require('util'),
        jwt = require('jsonwebtoken'),
        Email = require('../services/mail'),
        {uuid} = require('uuidv4'),
        User = require('../models/user'),
        Payment = require('../models/payment'),
        Service = require('../models/service'),
        {roleConstant} = require('../constants/index'),
        Subscription = require('../models/subscription'),
        {validationResult} = require('express-validator'),
        AppError = require('../utils/appError'),
        utility = require('../services/utility');


const createToken = id => {
    return jwt.sign({
        id
    }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
};

exports.login = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    try {
        const {email, password, role} = req.body;

        // 2) check if user exist and  password is correct
        const data = await User.findOne({email}).select('+password')

        if (!data || !await data.correctPassword(password, data.password)) {
            return next(new AppError(406, 'Not acceptable', 'Email/Password is incorrect'), req, res, next);
        }

        // 3) check if user is active
        if(data.isActive === false) {
            return next(new AppError(401, 'Unauthorized', 'You cannot access this account. See the administrator'), req, res, next);
        }
        
        if(role !== data.role){
            return next(new AppError(403, 'fail', 'Email/Password is incorrect'), req, res, next);
        }
        
        // 4) All correct, send jwt to client
        const token = createToken(data.id);

        let subscription = {};
        let payment = {};
        // 5) Add personal details to user
                
        const service = await Service.find({userID: data._id}) 

        if(data.role === roleConstant.CUSTOMER) {
            subscription = await getUserSubscriptions(data._id);
            payment = await Payment.find({userID: data._id});
        }
        else if(data.role === roleConstant.COMPANY) {

            subscription = await getUserSubscriptions(data._id);

            payment = await Payment.find().populate({
                path: 'userID',
                match: {userID: data._id},
                sort: {createdAt: -1}
            }); 
        }

        // add parent details
        let user = {};

        if(data.parentID !== null){
            user = await User.findById(data.id).populate({path: 'parentID', select: '_id firstname lastname email phone isActive children'});
        }

        //5)Implement session
        if(data) {
            req.session.userID = data._id

        // Remove the password from the output 
        data.password = undefined;
        
        res.status(200).json({
            status: 'success',
            token,
            user,
            documents: data.documents,      
            service,
            subscription
        });
    }
    } catch (error) {
        next(error);
    }
};

const getUserSubscriptions =  async (userId) => {
    const subscription =  await Subscription.find({userID: userId}).populate({
        path: 'serviceID',
        sort: {createdAt: -1},
        select: 'productID valueAmount valueType userID',
        populate:[{
            path: 'productID',
            select: 'name'
        },
        {
            path: 'userID',
            select: 'company_details.company_name'
        }]
    }); 
    if(!subscription && !subscription.userID) {
        return next(new AppError(404, 'fail', 'this user is doesn\'t exist'))
    }
    return subscription;
}

exports.signup = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        const confirmationToken = await generateToken();       
        const temporary_password = utility.generateRandomCharacter(6);  

        let body = req.body;
        body.isActive = false;
        body.confirmation_token = confirmationToken; 
        body.password = req.body.password ? req.body.password : temporary_password;
        const user = await User.create(body);
        const token = createToken(user.id); 
          
        
        //5)Implement session
        if(user) {
            req.session.userID = user._id
        }
        
        let emailBody = "";
        let subject = "";

        if(req.body.userType === 'admin') {
            emailBody = '<p>Hello '+user.firstname+',</p><p> An admin account has just been created for you on GoInsure.</p><p> Login with the following details:</p><p>Url: '+process.env.BASE_URL+'/admin <br />Username: '+ user.email +'<br /> Password:'+temporary_password+'</p><p>You are advised to change your password once you login.</p><p>Regard.</p><p>Kindly click on the link below to activate your account.</p><p><a href="'+process.env.BASE_URL+'/user/confirm_account/'+confirmationToken+'">Accout Activation Link<a></p><p> GoInsure Team.</p>'; // HTML body
            subject = 'GoInsure Admin Account Created!';
        }
        else {
            emailBody = '<p>Hello '+user.firstname+',</p><p> Thank you for signing up on Go-Insure.</p><p> We are glad to have you onboard the go-insure cover.</p><p>Be free to explore our website for your cheapest insurance deals.</p><p>Kindly click on the link below to activate your account.</p><p><a href="'+process.env.BASE_URL+'/user/confirm_account/'+confirmationToken+'">Accout Activation Link<a></p>';
            subject = 'Welcome to GoInsure!';
        }

        await Email.sendMail(user.email, subject, emailBody);
        user.password = undefined;

        res.status(200).json({
            status: 'success',
            token,
            user            
        });    

    } catch (error) {
        next(error);
    }

};

const generateToken = async () => {  
    try{
      const token = uuid();    
      const model = await User.findOne({confirmation_token: token});
      if(model) {
        this.generateToken();
      }
    
      return token;
    }
    catch(error) {
      return error;
    }    
}

exports.logout = async (req, res, next) => {
    try {
        console.log(req.session)
        const logout = req.session.destroy();
        if(!logout) {
            return next(new AppError(417, 'Expectation failed', 'unable to logout from this account'), req, res, next);
        }
        const cookie = res.clearCookie(process.env.SESS_NAME);
         if(!cookie) {
            return next(new AppError(417, 'Expected failed', 'unable to clear cookie from this account'), req, res, next);
        }
        res.status(200).json({
            status: 'you have successfully logged out of your account'
        })

    }catch (err) {
        next(err);
    }
}

exports.protect = async (req, res, next) => {
    try {
        // 1) check if the token is there
        let token;
        if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
            token = req.headers.authorization.split(' ')[1];
        }
        if (!token) {
            return next(new AppError(401, 'fail', 'You are not logged in! Please login in to continue'), req, res, next);
        }

        // 2) Verify token 
        const decode = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

        // 3) check if the user is exist (not deleted)
        const user = await User.findById(decode.id);
        if (!user) {
            return next(new AppError(401, 'fail', 'This user no longer exists'), req, res, next);
        }

        req.user = user;
        next();

    } catch (err) {
        next(err);
    }
};

// Authorization check if the user have rights to do this action
exports.restrictTo = (...permission) => {
    return (req, res, next) => {
        let result = req.user.permission.some(i => permission.includes(i));

        if (!result) {
            return next(new AppError(403, 'fail', 'You are not allowed to do this action'), req, res, next);
        }
        next();
    };
};