const User = require('../models/user'),
    Subscription = require('../models/subscription'),
    Service = require('../models/service'),
    Payment = require('../models/payment'),
    {validationResult} = require('express-validator');

exports.countEntities = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {

        const company_users = await User.countDocuments({parentID: req.query.id})+1;
        const Insurance_Services = await Service.countDocuments({userID: req.query.id});
        const Total_Subscription = await Subscription.find({userID: req.query.id}).countDocuments();
        const Active = await Subscription.countDocuments({userID: req.query.id, isActive: {$in: [true]}});
        const Inactive = await Subscription.countDocuments({userID: req.query.id, isActive: {$in: [false]}});
        const Payments = await Payment.countDocuments({userID: req.query.id});

        res.status(200).json({
            status: 'success',
            Company:{
              company_users,
              Insurance_Services,
              Subscriptions:{
                Total_Subscription,
                Active,
                Inactive
              },
              Payments
            }
        });

    } catch (error) {
        next(error);
    }

};