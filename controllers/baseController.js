const AppError = require('../utils/appError');
const  {validationResult} = require('express-validator');

exports.deleteOne = Model => async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {
        const data = await Model.findByIdAndDelete({_id: req.params.id});
        if (!data) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }

        res.status(200).json({
            status: 'This document has been deleted'
        });
    } catch (error) {
        next(error);
    }
};

exports.updateOne = Model => async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {

        if(req.params.id !== req.body._id) {
            return next(new AppError(404, 'fail', 'The id provided doesn\'t match the user id you are trying to access'), req, res, next);
        }; 

        const data = await Model.findByIdAndUpdate(req.params.id, {$set: req.body}, {
            new: true,
            runValidators: true,
        });
        if (!data) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        };

        res.status(200).json({
            status: 'updated',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.createOne = Model => async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        const data = await Model.create(req.body);

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.getOne = Model => async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };

    try {
        const data = await Model.findById(req.params.id);
        if(!data) {
          return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }

        res.status(200).json({
            status: 'success',
            data
        });
    } catch (error) {
        next(error);
    }
};

exports.getAll = Model => async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        const data = await Model.find();

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }

};

exports.count = Model => async (req, res, next) => {
    try {
        const data = await Model.estimatedDocumentCount({});

        if(data > 0){
            res.status(200).json({
            status: 'success',
            data
            });
        }
        else if(data === 0){
            res.status(200).json({
                status: 'This storage is empty'
            })
        }
    } catch (error) {
        next(error);
    }

};

exports.export = Model => async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        const data = await Model.find({}, req.body.fields);

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.search = (Model, config=null) => async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        var page = (req.body.page) ? req.body.page : 1;
        var perPage = (req.body.limit) ? req.body.limit :10;
        var query = req.body.query || {};
        
        var options = {
            sort: req.body.sort || {createdAt: -1},            
            lean: true,
            page: page, 
            limit: perPage
        };

        if(config && config.populate) {
            options.populate = config.populate;
        }
        
        const data = await Model.paginate(query, options); 

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.advance_search = (Model, config=null) => async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        var page = (req.body.page) ? req.body.page : 1;
        var perPage = (req.body.limit) ? req.body.limit :10;
        var query = {};        

        //console.log("Got here")

        if(req.body.query) {
            for (const [key, value] of Object.entries(req.body.query)) {                
                //console.log(`${key}:${value}`)
                //if the object's value is an object
                if(Object.prototype.toString.call(value) === '[object Object]') {
                    //console.log("Value1",value)
                    const nested_object = Object.entries(value);
                    const nested_key = nested_object[0][0];
                    const nested_value = nested_object[0][1];
                   // console.log("obj",nested_object[0][0])

                    query[`${key}.${nested_key}`] = {'$regex': nested_value, '$options':'i'};   

                }
                else if(Number.isFinite(value)) {                   
                    query[key] = value;
                }
                else if(value){
                    query[key] = {'$regex': value, '$options':'i'};
                }

                //console.log("query", query)
                
            }  
        }

        var options = {
            sort: req.body.sort || {createdAt: -1},            
            lean: true,
            page: page, 
            limit: perPage
        };
        if(config && config.populate) {
            options.populate = config.populate;
        }
        
        const data = await Model.paginate(query, options); 

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.deleteFromRelation = (Model, param={}) => async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    Model.deleteMany(param, function (err) {
        if(err) {
          return next(new AppError(404, 'fail', 'Could not delete model\'s relations'), req, res, next);
        }
    });
};