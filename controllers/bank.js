const base = require('./baseController'),
    {validationResult} = require('express-validator'),
    AppError = require('../utils/appError'),
    Bank = require('../models/bank');

exports.createBankAccount = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    
    try {
        const bank = await Bank.findOne({name: req.body.name});

        if(bank && bank.name){
          return next(new AppError(422, 'fail', 'This bank already exists'), req, res, next);
        };

        const data = await Bank.create(req.body);        

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.getAllAccounts = base.getAll(Bank);
exports.getAccountDetail = base.getOne(Bank);

exports.updateAccount = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {

        if(req.params.id !== req.body._id) {
            return next(new AppError(404, 'fail', 'The id provided doesn\'t match the user id you are trying to access'), req, res, next);
        }; 

        const bank = await Bank.findOne({name: req.body.name});

        const data = await Bank.findByIdAndUpdate(req.params.id, {$set: req.body}, {
            new: true,
            runValidators: true,
        });
        if (!data) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        };

        res.status(200).json({
            status: 'updated',
            data
        });

    } catch (error) {
        next(error);
    }
};
 
exports.deleteAccount = base.deleteOne(Bank);
exports.searchAccount = base.search(Bank);
exports.adSearchAccount = base.advance_search(Bank);