const User = require('../models/user'),
    Subscription = require('../models/subscription'),
    Service = require('../models/service'),
    Payment = require('../models/payment'),
    Bank = require('../models/bank'),
    SettlementHistory = require('../models/settlementHistory'),
    jwt = require('jsonwebtoken'),
    base = require('./baseController'),
    Email = require('../services/mail'),
    { uuid } = require('uuidv4'),
    {roleConstant} = require('../constants/index'),
    utility = require('../services/utility'),
    fs = require('fs'),
    {validationResult, body} = require('express-validator'),
    AppError = require('../utils/appError'),
    { delete_file } = require('../services/upload.js'),
    axios = require('axios');     

const createToken = id => {
    return jwt.sign({
        id
    }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
};

exports.registerCompany = async (req, res, next) => {
   const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    try {
        
        const confirmationToken = await generateToken();  
        const temporary_password = utility.generateRandomCharacter(6);    
        
        const userDetail = await registerCompanyDetails(req.body, req.file, confirmationToken, temporary_password);

        const emailBody = '<p>Hello '+userDetail.firstname+',</p><p> Welcome to GoInsure, a platform that gives you access to hundreds of thousands of potential customers who are looking to buy affordable insurance policies without stress.</p><p>Please, refer to your sign in details below and activate your account.</p><p>Username: '+ userDetail.email +'</p><p>Your Temporary Password is: '+temporary_password +'</p><p><a href="'+process.env.BASE_URL+'/user/confirm_account/'+confirmationToken+'">Accout Activation Link<a></p>'; // HTML body
        const subject = 'Welcome to GoInsure!';

        // Mail User
        await Email.sendMail(userDetail.email, subject, emailBody);
        
        res.status(200).json({
            status: 'success',
            userDetail
        });

    } catch (error) {
        next(error);
    }
};

const registerCompanyDetails = async(details, file, confirmation_token, temporary_password) => {
    try {
        const key = file && file.location;
        let bank_name = "";

        if(details.bank_id){
            const bank = await Bank.findById(details.bank_id);
            bank_name = bank.name
        };
                    
        let body = {}
            body.firstname = details.firstname,
            body.lastname = details.lastname,
            body.email = details.email,
            body.phone = details.phone,
            body.role = details.role,
            body.isActive = false,
            body.permission = parseInt(details.permission),
            body.password = temporary_password,
            body.company_details = {
                company_name: details.company_name,
                company_address: details.company_address,
                company_email: details.company_email,
                company_phone: details.company_phone,
                logo_url: key,
                bank_details: {
                    bank_id: details.bank_id,
                    bank_name: bank_name,
                    account_name: details.account_name,
                    account_number: details.account_number
                }
            },
            body.confirmation_token = confirmation_token;

        const user = await User.create(body)

        return user;

    } catch (error) {
        return error;
    }
};

exports.registerCompanyAdmin = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    try {
        const confirmationToken = await generateToken();       
        const temporary_password = utility.generateRandomCharacter(6);    
        
        let body = req.body;
        body.isActive = false;
        body.password = temporary_password;
        body.confirmation_token = confirmationToken;
        
        const parent = await User.findById(req.body.parentID);
        if(!parent){
          return next(new AppError(404, 'fail', 'This Company Admin doesn\'t exist'), req, res, next);
        };

        if(parent.role !== roleConstant.COMPANY) {
          return next(new AppError(401, 'fail', 'Unauthorized. This is not a company account.'), req, res, next);
        };             

        if(req.body.role !== roleConstant.COMPANY) {
          return next(new AppError(401, 'fail', 'Unauthorized. You can only create an admin user'), req, res, next);
        };

        body.company_details = parent.company_details;
        const user = await User.create(body)

        const token = createToken(user.id);

        await User.findByIdAndUpdate(user.parentID, 
          { $push: {children: user}}, {new: true, runValidators: true}
        );
                    
        //5)Implement session
        if(user) {
          req.session.userID = user._id
        }
        
        const emailBody = '<p>Hello '+user.firstname+',</p><p> Welcome to GoInsure, a platform that gives you access to hundreds of thousands of potential customers who are looking to buy affordable insurance policies without stress.</p><p>Please, refer to your sign in details below and activate your account.</p><p>Username: '+ user.email +'</p><p>Your Temporary Password is: '+temporary_password +'</p><p><a href="'+process.env.BASE_URL+'/user/confirm_account/'+confirmationToken+'">Accout Activation Link<a></p>'; // HTML body
        const subject = 'GoInsure Company Admin Account!';

        // Mail User
        await Email.sendMail(user.email, subject, emailBody);

        user.password = undefined;

        res.status(200).json({
            status: 'success',
            token,
            user
        });    

    } catch (err) {
        next(err);
    }

};

const generateToken = async () => {  
    try{
      const token = uuid();    
      const model = await User.findOne({confirmation_token: token});
      if(model) {
        return this.generateToken();
      }
    
      return token;
    }
    catch(error) {
      return error;
    }    
};

exports.confirm_account = async(req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try{  
        const user = await User.findOne({confirmation_token: req.params.random_character});
        
        if(user && user.isActive === true) {
            return next(new AppError(401, 'Your account has been activated already'), req, res, next);
        } else if(!user) {
            return next(new AppError(401, 'User not found'), req, res, next);
        }
          
        const data = await User.findOneAndUpdate({confirmation_token: req.params.random_character}, {$set: {isActive: true}}, {
            new: true,
            runValidators: true,
        });
        
        const parent = await User.findOne({_id: data.parentID});
        
        if(parent){
            await removeUserFromParent(parent, data._id);
            // await User.findByIdAndUpdate(parent._id, 
            //     { $push: {children: data}}, {new: true, runValidators: true}
            // );
            parent.children.push(data)
            await parent.save();
        }

        if (!data) {
          return next(new AppError(404, 'fail', 'This confirmation token doesn\'t exist'), req, res, next);
        };

        const token = createToken(data.id); 

        res.status(200).json({
            status: 'your account has been activated successfully',
            data,
            token
        });
        
    }catch(err) {
        next(err);
    }
};

exports.getAccountName = async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
  };
  try {
    const data = {
        account_number: req.body.account_number,
        account_bank: req.body.account_bank
    }
    const key = (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') ? process.env.FLUTTERWAVE_KEY : process.env.FLUTTERWAVE_TEST_KEY;

    const response = await axios.post(process.env.FLUTTERWAVE_GETACCOUNTNAME, data, {
        headers: { 
        "Authorization": key
        }
    });
    const accountdetails = response.data

    res.status(200).json({
        accountdetails
    })
  } catch (error) {
        return next(new AppError(422, 'fail', 'The account number or bank code is incorrect'), req, res, next);
    }
};

// exports.transfer = async (req, res, next) => {
//     const errors = validationResult(req);
  
//     if (!errors.isEmpty()) {
//         return res.status(422).json({ errors: errors.array() });
//     };
//     try {
//         const settlement = await SettlementHistory.findOne({paymentID:req.body.paymentID});

//         if(settlement){
//             return next(new AppError(404, 'fail', 'This payment has already been settled'), req, res, next);
//         };

//         let data = {};
//           data.account_bank = req.body.account_bank
//           data.account_number = req.body.account_number
//           data.amount = req.body.amount
//           data.narration = req.body.narration
//           data.currency = req.body.currency
//           data.reference = req.body.reference
//           data.debit_currency = req.body.debit_currency

//         const transfer_details = await axios.post('https://api.flutterwave.com/v3/transfers', data, {
//             headers: { 
//             "Authorization": process.env.FLUTTERWAVE_KEY 
//             }
//         });
        
//             const payment = await Payment.findById(paymentID);
//             const amountPaidByCustomer = parseInt(payment.amount);
//             const serviceId = payment.serviceID;
//             const userId = payment.userID;
//             const service = await Service.findById(serviceId);
//             const user = await User.findById(userId);
//             const companyAmount = (parseInt(service.companyRate)/100)*amountPaidByCustomer;

//             let doc = {};
//                 doc.paymentID = paymentID;
//                 doc.company_amount = companyAmount;
//                 doc.company_name = user.company_details['company_name'];
//                 doc.total_amount = amountPaidByCustomer;
//                 doc.GoInsure_amount = parseInt(doc.total_amount)-parseInt(doc.company_amount);

//         const transfer_result_status = transfer_details.data.status;

//         if(transfer_result_status === 'success'){
//            let settled = await SettlementHistory.create(doc);
//             console.log('This payment has been settled ====>', settled);
//         };

//         const payment_update = await Payment.findByIdAndUpdate(req.body.paymentID, {$set: {isSettled: true}}, {
//             new: true,
//             runValidators: true,
//         });
  
//         res.status(200).json({
//             payment_update
//         });
        
//     } catch (error) {
//         next(error);
//     }
// };
  
exports.getAllUser = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {

        var page = (req.query.page) ? req.query.page : 1;
        var perPage = (req.query.limit) ? req.query.limit :10;

        var options = {
            sort: req.query.sort || {createdAt: -1},            
            lean: true,
            page: page, 
            limit: perPage
        };

        const data = await User.paginate({parentID: null}, options);

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }

};

exports.forgot_password = async (req, res, next) =>{
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try{
        const random_character = await generateToken();
        const user = await User.findOneAndUpdate({email:req.query.email, role:req.query.role_id}, {$set: {remember_token: random_character}}, {
            new: true,
            runValidators: true,
        });

        if(!user){
            return next(new AppError(403, 'fail', 'The email does not exist'), req, res, next);
        };
        
        const emailBody = '<p>Hello '+user.firstname+',</p><p> You recently requested to change your password.</p><p> If you did not make this request, kindly ignore this email.</p><p>To change your password, click on the link below</p><p><a href="'+process.env.BASE_URL+'/user/reset_password/'+random_character+'">Reset Password Link<a></p>'// HTML body
        const subject =  'Password Reset Request' // Subject line
        
        // Mail User
        await Email.sendMail(user.email, subject, emailBody);

        res.status(200).json({
                status: 'A reset link has been sent to your email'
            });

    }catch(error){
        next(error);
    }
};

/* exports.testEmail = async (req, res, next) => {
    
    // Mail User
    try {
        await Email.sendMail('geebengs@yahoo.com', 'Email Test', '<b>Email Testing Testing</b>');

        res.status(200).json({
            status: 'A reset link has been sent to your email'
        });
    }
    catch(error) {
        next(error);
    }
    
} */

exports.updateUser = async (req, res, next) => {

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    if(req.params.id !== req.body._id) {
      return next(new AppError(406, 'Not acceptable', 'the route id does not match the supplied id'), req, res, next);
    };

    if(req.body.password) {
      req.body.password = await utility.hashPassword(req.body.password);
    }

    if(req.body.permission) {
        req.body.permission = parseInt(req.body.permission);
    }
    
    try {
        const data = await User.findOneAndUpdate ({_id: req.params.id}, {$set: req.body}, {new: true, runValidators: true});
        console.log("w", data)
        // get the parent data 
        const user = await User.findOne({_id: data.parentID});
        console.log('user', user);
        if(user) {
            await removeUserFromParent(user, data._id);
            await User.findByIdAndUpdate(user._id, 
                { $push: {children: data}}, {new: true, runValidators: true}
            );
        }
        if (!data) {
          return next(new AppError(404, 'Not found', 'No document found with that id'), req, res, next);
        };

        res.status(200).json({
            status: 'updated',
            data
        }); 
    }
    catch (error) {
        next(error);
    }    
        
};

const removeUserFromParent = (user, id) => {
    //remove user as a child of its parent.
    if(user.children && user.children.length > 0) {
        User.update(
            {_id: user._id},
            { "$pull": { "children": {_id: id}}},
            { "multi": true },
            function(err,status) {
                if(err) {
                    return next(new AppError(417, 'Expectation failed', 'Could not delete child of this user'), req, res, next);
                }
            }
        )
    }
};

exports.updateCompany = async (req, res, next) => {
    
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const path = req.file && req.file.location;

    if(req.params.id !== req.body._id) {
      return next(new AppError(404, 'fail', 'the id does not match the supplied id'), req, res, next);
    };

    const users = await User.findById(req.body._id);
    const bankDetails = users.company_details['bank_details'];

    if(req.body.password) {
        req.body.password = utility.hashPassword(req.body.password);
    }
    
    if(req.body.permission) {
        req.body.permission = parseInt(req.body.permission);
    }

    req.body.isActive = req.body.isActive || true;
    req.body.company_details = {
        company_name: req.body.company_name,
        company_address: req.body.company_address,
        company_email: req.body.company_email,
        company_phone: req.body.company_phone,
        logo_url: path,
        bank_details: bankDetails
    }  

    try {
        const data = await User.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true, runValidators: true});

        if (!data) {
            return next(new AppError(403, 'fail', 'No document found with that id'), req, res, next);
        };

        res.status(200).json({
            status: 'updated',
            data
        });
    }
    catch (error) {
        next(error);
    }
    
};

exports.addBankDetail = async (req, res, next) => {
    
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    };

    if(req.params.id !== req.body._id) {
        return next(new AppError(404, 'fail', 'the id does not match the supplied id'), req, res, next);
    };

    let user = await User.findById(req.body._id);

    if(user.role !== roleConstant.COMPANY){
        return next(new AppError(404, 'unauthorized', 'only a company can add bank details'), req, res, next);
    };

    const userBank = user.company_details['bank_details'].account_number;

    if(userBank === req.body.account_number) {
        return next(new AppError(404, 'unauthorized', 'the bank account already exist'), req, res, next);
    };

    try {       
        const subAccount = await addSubAccount(req.body, req.params, next);  

            if(subAccount.data.status === 'success') {
                const bank = await Bank.findById(req.body.bank_id);
                const bank_name = bank && bank.name;
            
                let company_details = user.company_details;
                company_details['bank_details'] = {
                    flutterSubAccount_id: subAccount.data.data.subaccount_id,
                    bank_id: req.body.bank_id,
                    account_name: req.body.account_name,
                    account_number: req.body.account_number,
                    bank_name: bank_name,
                    bank_code: req.body.bank_code
                }
    
            user['company_details'] = company_details;  
        
            const data = await User.findByIdAndUpdate(req.params.id, {$set: user}, {new: true, runValidators: true});
            
            if (!data) {
                return next(new AppError(403, 'fail', 'No document found with that id'), req, res, next);
            };  
        
            res.status(200).json({
                status: 'updated',
                data
            });
        }    
    }
    catch (error) {
        return next(new AppError(403, 'fail', error+': unable to add bank details'), req, res, next);
    }
};

const addSubAccount = async(details, param, next) => {
    try {
        const business = await User.findById(param.id);
        const businessName = business.company_details['company_name'];
        const businessMobile = business.company_details['company_phone'];
        const businessEmail = business.company_details['company_email'];
    
        let data = {};
        data.account_bank = details.bank_code
        data.account_number = details.account_number
        data.business_name = businessName
        data.business_email = businessEmail
        data.country = "NG"
        data.split_value = 0.85
        data.business_mobile = businessMobile
        data.business_contact = business.firstname
        data.business_contact_mobile = business.phone
        data.split_type = "percentage"
        data.meta = details.meta
        
        const key = (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') ? process.env.FLUTTERWAVE_KEY : process.env.FLUTTERWAVE_TEST_KEY;
        const subAccount = await axios.post(process.env.FLUTTERWAVE_SUBACCOUNT, data, {
            headers: { 
            "Authorization": key 
            }
        });
    
        return subAccount;
    
    } catch (error) {
        return next(new AppError(403, 'fail', 'This bank account already exists. Enter another one.'));
    }
}

exports.documentUpload = async (req, res, next) =>{
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  };

  if(req.params.id !== req.body._id) {
    return next(new AppError(406, 'Not Acceptable', 'The id provided doesn\'t match the user id you are trying to access'), req, res, next);
  };

  try {
    let doc = {};
    const path = req.file && req.file.location;

    if(path) {
        doc.field_name = req.body.field_name;
        doc.file = path;
    }    

    const data = await User.findByIdAndUpdate(req.params.id, 
        { $push: {documents: doc,}}, {$push: {documents: { field_name: req.body.field_name}}}, {new: true, runValidators: true}
    )
    console.log("data",data)

    if(req.file.size > 1024 * 1024){
      return next(new AppError(404, 'fail', 'File upload too large!!! image size must not exceed 1mb'), req, res, next);
    };

    if (!data) {
      return next(new AppError(404, 'Not found', 'No document found with that id'), req, res, next);
    };

    res.status(200).json({
        status: 'uploaded',
        data
    });

} catch (error) {
    next(error);
}

};

// exports.kycForm = async (req, res, next) =>{
//   const errors = validationResult(req);

//   if (!errors.isEmpty()) {
//     return res.status(422).json({ errors: errors.array() });
//   }; 

//   if(req.params.id !== req.body._id) {
//     return next(new AppError(401, 'unauthorized', 'The id provided doesn\'t match the user id you are trying to access'), req, res, next);
//   };

//   try {
//     const path = req.file && req.file.path;

//     if(req.file.size > 1024 * 1024) {
//       return next(new AppError(404, 'fail', 'File upload too large!!! image size must not exceed 1mb'), req, res, next);
//     };

//     const user = await User.findById(req.body._id);

//     if(!user){
//       return next(new AppError(404, 'Not found', 'The id provided doesn\'t exist'), req, res, next);
//     };

//     const data = await User.findByIdAndUpdate(req.params.id, 
//         { $set: {company_details: {
//             company_name: user.company_details.company_name,
//             company_address: user.company_details.company_address,
//             company_email: user.company_details.company_email,
//             company_phone: user.company_details.company_phone,
//             logo_url: user.company_details.logo_url,
//             kyc_form: path
//         }}}, {new: true, runValidators: true}
//     )

//     if (!data) {
//       return next(new AppError(404, 'Not found', 'No document found with that id'), req, res, next);
//     };

//     res.status(200).json({
//         status: 'uploaded',
//         data
//     });

// } catch (error) {
//     next(error);
// }

// };

exports.delete_file = async (req, res, next) =>{
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try{
        if(req.params.user_id != req.body.user_id) {
          return next(new AppError(401, 'Unauthorized', 'The route parameter does not match user_id in the body'), req, res, next);
        }

        User.findOneAndUpdate(
            {_id:req.params.user_id},
            { $pull: { documents: {field_name: req.body.field_name}}},
            { new: true },
            async function(err, updated_user) {
              if(err) {
                return next(new AppError(417, 'Expectation failed', 'Could not delete document'), req, res, next);
              }

               delete_file(req.body.file);
              
              res.status(200).json({
                status: 'This file has been deleted',
                user:updated_user
              });

            }
        )

    }
    catch(error) {
        next(err);
    }
}

exports.reset_password = async (req, res, next) =>{
    const errors = validationResult(req);

    if(!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };

    try{
        const hash = await utility.hashPassword(req.body.password);
        const data = await User.findOneAndUpdate({remember_token: req.params.random_character}, {$set: {password: hash}}, {
            new: true,
            runValidators: true,
        });

        if (!data) {
            return next(new AppError(404, 'fail', 'Invalid user id'), req, res, next);
        };

        if (req.params.random_character !== data.remember_token) {
            return next(new AppError(404, 'fail', 'Invalid token'), req, res, next);
        };

        res.status(200).json({
            status: 'your password reset was successful',
            data
        });
    }catch(error) {
        next(error);
    }
};


exports.change_password = async (req, res, next) =>{
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    };

    try{
        const {id, old_password} = req.body
        const user = await User.findOne({_id: id}).select('+password');
        if(user.isActive === false) {
            return next(new AppError(409, 'Conflict', 'This account is not activated'), req, res, next);
        }
            console.log("1", user)
        if (!user || !await user.correctPassword(old_password, user.password)) {
          return next(new AppError(409, 'Conflict', 'user id/old password is incorrect'), req, res, next);
        };

        const hash2 = await utility.hashPassword(req.body.password);
        const data = await User.findByIdAndUpdate( req.params.id, {$set: {password: hash2}}, {
            new: true,
            runValidators: true,
        });

        if (!data) {
          return next(new AppError(404, 'Not found', 'No user was found with the id'), req, res, next);
        };

        const token = createToken(user.id);

        res.status(200).json({
            status: 'your password change was successful',
            data,
            token
        });
    }catch(error) {
        next(error);
    }
};

exports.deleteUser = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    };
    try {
        const userID = req.params.id;
        const user = await User.findById(req.params.id).populate('parentID');
        console.log(req.params)
        console.log("ko", user)
        if (!user) {
            return next(new AppError(404, 'Not found', 'No document found with that id'), req, res, next);
        }        

        
        deleteUserFromParent(user, userID); 
        base.deleteFromRelation(Subscription, {userID});
        base.deleteFromRelation(Service,{userID});
        base.deleteFromRelation(Payment,{userID});

        await User.findByIdAndDelete(req.params.id);

        res.status(200).json({
            status: 'This user has been deleted'
        });
    } catch (error) {
        next(error);
    }
};

const deleteUserFromParent = (user, id) => {
    //remove user as a child of its parent.
    if(user.parentID && user.parentID.children && user.parentID.children.length > 0) {
        User.update(
            {_id: user.parentID},
            { "$pull": { "children": {_id: id}}},
            { "multi": true },
            function(err,status) {
                if(err) {
                    return next(new AppError(417, 'Expectation failed', 'Could not delete child of this user'), req, res, next);
                }
            }
        )
    }
};

exports.exportUser = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        const data = await User.find({role: req.body.role, parentID: req.body.parentID}, req.body.fields);

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }

};

exports.getUser = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };

    try {
        const data = await User.findById(req.params.id);
        if(!data) {
          return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }

        res.status(200).json({
            status: 'success',
            data
        });
    } catch (error) {
        next(error);
    }
};

exports.searchUser = base.search(User);
exports.advanceUserSearch = base.advance_search(User);