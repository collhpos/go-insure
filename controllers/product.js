const Product = require('../models/products'),
        base = require('./baseController'),
        {roleConstant} = require('../constants/index'),
        Service = require('../controllers/service'),
        {validationResult} = require('express-validator'),
        AppError = require('../utils/appError');


exports.createProduct = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    
    try {
        const product = await Product.findOne({name: req.body.name});

        if(product && product.name){
          return next(new AppError(422, 'fail', 'This product name already exists'), req, res, next);
        };

        const data = await Product.create(req.body);        

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.getProduct = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    
    try {
        const data = await Product.findById(req.params.id).populate('parentID');

        if (!data) {
          return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        };

        res.status(200).json({
            status: 'success',
            data
        });
    } catch (error) {
        next(error);
    }
};

const populateOption = {
    populate:{
        path:'parentID'
    }
};

exports.getAllProducts = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        var page = (req.query.page) ? req.query.page : 1;
        var perPage = (req.query.limit) ? req.query.limit :10;

        var options = {
            sort: req.query.sort || {createdAt: -1},
            populate: populateOption.populate,
            lean: true,
            page: page, 
            limit: perPage
        };

        const data = await Product.paginate({published:true}, options);
        //
        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.deleteProduct = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        const data = await Product.findByIdAndDelete(req.params.id);
        const productID = req.params.id;

        if (!data) {
          return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }

        base.deleteFromRelation(Service, {productID});

        res.status(200).json({
            status: 'This document has been deleted'
        });
    } catch (error) {
        next(error);
    }
};


exports.updateProduct = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {

        if(req.params.id !== req.body._id) {
            return next(new AppError(404, 'fail', 'The id provided doesn\'t match the user id you are trying to access'), req, res, next);
        }; 

        if(req.body._id === req.body.parentID) {
            return next(new AppError(404, 'fail', 'The parent id cannot be the same with the product id'), req, res, next);
        };

        const product = await Product.findOne({name: req.body.name});

        const data = await Product.findByIdAndUpdate(req.params.id, {$set: req.body}, {
            new: true,
            runValidators: true,
        });
        if (!data) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        };

        res.status(200).json({
            status: 'updated',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.exportProducts = base.export(Product);
exports.searchProduct = base.search(Product, populateOption);
exports.advanceProductSearch = base.advance_search(Product, populateOption);