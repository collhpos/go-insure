exports.getPayments = {
    tags: ['Payment'],
    summary: "Get All Payments",
    description: "Returns all Payments from the system",
    operationId: 'getPayments',
    security: [
        {
            bearerAuth: []
        }
    ],
    responses: {
      '200': {
        description: 'Payments were obtained',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Payments'
            }
          }
        }
      },
      '400': {
        description: 'Missing parameters',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Error'
            },
            example: {
              message: 'id is missing',
              internal_code: 'missing_parameters'
            }
          }
        }
      }
    }
} 

exports.getPayment = {
  tags: [
    "Payment"
  ],
  summary: "Get Payment by id",
  operationId: "getPaymentById",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "The id of the Payment that needs to be fetched. ",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    200: {
      description: "successful operation",
      content: {
        "application/xml": {
          schema: {
            $ref: "#/components/schemas/Payment"
          }
        },
        "application/json": {
          schema: {
            $ref: "#/components/schemas/Payment"
          }
        }
      }
    },
    400: {
      description: "Invalid Payment_id supplied",
      content: {}
    },
    404: {
      description: "ID not found",
      content: {}
    }
  }
}

exports.exportPayment = {
  tags: [
      'Payment'
    ],
    summary: "export Payment",
    description: "export Payment",
    operationId: "exportPayment",
    requestBody: {
      description: "example of string : firstname lastname",
      content: {
        'application/json': {
          schema: {
            properties:{
              fields: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      },
      422: {
        description: 'validation error'
      }
  }
};

exports.getPaymentsByCompany = {
  tags: [
    "Payment"
  ],
  summary: "Get payments by company id",
  operationId: "getPaymentsByCompanyId",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "The id of the company that needs to be fetched. ",
      required: true,
      schema: {
        type: "string",
      }
    }
  ],
  responses: {
    200: {
      description: 'Payments were obtained',
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/Payments'
          }
        }
      }
    },  
    400: {
      description: "error"
    },
    404: {
      description: "No company document found with that id"
    }
  }
};

exports.postPayment = {
    tags: [
        'Payment'
      ],
      summary: "Create Payment",
      description: "Create a new Payment detail.",
      operationId: "createPayment",
      requestBody: {
        description: "Created Payment object",
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Payment'
            }
          }
        },
        required: true
      },
      responses: {
        '200': {
          description: 'New Payment created'
        },
        '400': {
          description: 'Invalid parameters',
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/Error'
              },
              example: {
                message: 'User id 10, 20 already exist',
                internal_code: 'invalid_parameters'
              }
            }
          }
        }
    }
}

exports.advancedPaymentSearch = {
  tags: [
      'Payment'
    ],
    summary: "Advanced Search payment",
    description: "Search the Payment Table.",
    operationId: "advancedPaymentSearch",
    requestBody: {
      description: "found searched documents",
      content: {
        'application/json': {
          schema: {
            properties: {
              query: {
                type: "object",
                properties: {
                  field: {
                    type: "string"
                  }
                }
              },
              limit : {
                type: "number",
                default: 10
              },
              sort: {
                properties: {
                  field: {
                    type: "number",
                    enum: "-1, 1"
                  }
                }
              },
              select: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      '200': {
        description: 'database search is successful'
      },
      '400': {
        description: 'Invalid parameters',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Error'
            },
            example: {
              message: 'Payment id 10, 20 already exist',
              internal_code: 'invalid_parameters'
            }
          }
        }
      }
  }
}

exports.searchPayment = {
    tags: [
        'Payment'
      ],
      summary: "Search payment",
      description: "Search the database.",
      operationId: "searchPayment",
      requestBody: {
        description: "found searched documents",
        content: {
          'application/json': {
            schema: {
              properties: {
                query: {
                  type: "object",
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: "-1, 1"
                    }
                  }
                },
                select: {
                  type: "string"
                }
              }
            }
          }
        },
        required: true
      },
      responses: {
        '200': {
          description: 'database search is successful'
        },
        '400': {
          description: 'Invalid parameters',
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/Error'
              },
              example: {
                message: 'Payment id 10, 20 already exist',
                internal_code: 'invalid_parameters'
              }
            }
          }
        }
    }
}

exports.updatePayment = {
  tags: [
    "Payment"
  ],
  summary: "Update Payment",
  description: "This can only be done by the logged in user.",
  operationId: "updatePayment",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of Payment that needs to be updated",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "Updated Payment's object",
    content: {
      "*/*": {
        schema: {
          properties: { 
            _id: {
              type: "string"
            },
            userID: {
              type: "string"
            },              
            subscriptionID: {
              type: "string"
            },
            serviceID: {
              type: "string"
            },
            amount: {
              type: "string",
            },
            trxRef: {
              type: "string",
            },
            authorizationCode: {
              type: "string"
            }
          }
        }
      }
    },
    required: true
  },
  responses: {
    400: {
      description: "Invalid Payment's id supplied",
      content: {}
    },
    404: {
      description: "id not found",
      content: {}
    }
  },
  "x-codegen-request-body-name": "body"
}

exports.deletePayment = {
  tags: [
    "Payment"
  ],
  summary: "Delete Payment",
  description: "This can only be done by the logged in user.",
  operationId: "deletePayment",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of Payment that needs to be deleted",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    400: {
      description: "Invalid Payment's id supplied",
      content: {}
    },
    404: {
      description: "id not found",
      content: {}
    }
  },
}