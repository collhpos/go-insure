exports.countEntities = {
    tags: ['DashboardEntities'],
    summary: "Count documents",
    description: "Returns the sum of documents in the system",
    operationId: 'countEntities',
    security: [
        {
            bearerAuth: []
        }
    ],
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      }
    }
} 
