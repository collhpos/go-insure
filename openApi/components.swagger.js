exports.components = {
  schemas: {            
    User: {
      type: 'object',
        properties: {               
            firstname: {
              type: "string"
            },
            lastname: {
              type: "string"
            },
            email: {
              type: "string"
            },
            password: {
              type: "string"
            },
            phone: {
              type: "string"
            },
            role: {
              type: "string",
              enum: ["1", "2"]
            },                 
            isActive: {
              type: "boolean",
              description: "User Status"
            },
            confirmation_token: {
              type: "string"
            },
            remember_token: {
              type: "string"
            },
            parentID: {
              type: "string"
            },
            userType: {
              type: "string",
            }                 
        }
    },  
    Company: {
      type: 'object',
        properties: {               
            firstname: {
              type: "string"
            },
            lastname: {
              type: "string"
            },
            email: {
              type: "string"
            },
            password: {
              type: "string"
            },
            phone: {
              type: "string"
            },
            role: {
              type: "string",
              enum: ["1", "2"]
            },                 
            isActive: {
              type: "boolean",
              description: "User Status"
            },              
            company_name: {
              type: "string"
            },
            company_address: {
              type: "string"
            },
            company_email: {
              type: "string"
            },
            company_phone: {
              type: "string"
            },
            logo_url: {
              type: "string"
            },
            bank_id: {
              type: "MongoId"
            },
            account_name: {
              type: "string"
            },
            account_number: {
              type: "number"
            },
            confirmation_token: {
              type: "String"
            },
            remember_token: {
              type: "String"
            },
            children: {
              type: 'array',
              properties: {
                firstname: {
                  type: "String",
                },
                lastname: {
                  type: "String",
                },
                email: {
                  type: "String",
                },       
                phone: {
                  type: "String",
                },        
                isActive: {
                  type: "Boolean",
                  default: false,
                }
              }          
            },
            documents: {
              type: "array"
            },
            userType: {
                type: "String",
            }                                   
        }
    }, 
    Subscription: {
      type: 'object',
        properties: {               
            userID: {
              type: "string"
            },
            serviceID: {
              type: "string"
            },
            startDate: {
              type: "string"
            },
            duration: {
              type: "string",
              enum: ["1", "2"]
            },
            phone: {
              type: "string"
            },
            period: {
              type: "string",
            },
            endDate: {
              type: "string"
            },
            subscriber_data: {
              type: 'object',
              properties: {   
                name: {
                  type: "string"
                }  
              }          
            },
            documents_names: {
              type: 'object',
              properties: {   
                name: {
                  type: "string"
                }  
              }          
            },
            document_types: {
              type: 'object',
              properties: {   
                name: {
                  type: "string"
                }  
              }          
            },
            documents: {
              type: 'object',
              properties: {   
                name: {
                  type: "string"
                }  
              }          
            },
            certificateURL: {
              type: "string"
            },   
            isActive: {
              type: "boolean",
              description: "subscription Status",
              enum: [true, false]
            },              
            isPaid: {
              type: "boolean",
              description: "Payment Status",
              enum: [true, false]
            }
        }
    },   
    Product: {
      type: 'object',
        properties: {         
            id: {
              type: "number"
            },      
            name: {
              type: "string"
            },
            description: {
              type: "string"
            },
            parentID: {
              type: "string"
            },
            has_parent: {
              type: "boolean",
              enum: [true, false]
            },
            has_child: {
              type: "boolean",
              enum: [true, false]
            },
            requirement: {
              type: "mixed",
            },
            useParentRequirement: {
              type: "boolean",
              enum: [true, false]
            },
            isActive: {
              type: "boolean",
              description: "Company product Status",
              enum: [true, false]
            },
            published: {
              type: "boolean",
              enum: [true, false]
            },
        }
    },  
    Payment: {
      type: 'object',
        properties: { 
            userID: {
              type: "string"
            },              
            subscriptionID: {
              type: "string"
            },
            serviceID: {
              type: "string"
            },
            amount: {
              type: "string",
            },
            trxRef: {
              type: "string",
            },
            authorizationCode: {
              type: "string"
            }
        }
    },
    Service: {
      type: 'object',
        properties: {               
            productID: {
              type: "string"
            },
            product_name: {
              type: "string"
            },
            parentID: {
              type: "string"
            },
            userID: {
              type: "string"
            },
            requirment: {
              type: "mixed",
            },
            summary: {
                type: 'string'
            },
            description: {
                type: 'string'
            },
            valueType: {
              type: "string",
              enum: ["1", "2"]
            },
            valueAmount: {
              type: "string",
            },
            duration: {
              type: "string",
              enum: ["1", "2"]
            },
            companyRate: {
              type: "string",
            },
            published: {
              type: "boolean",
              enum: [true, false]
            },
          }
        },  
        Bank: {
          type: 'object',
          properties: {
            name: {
              type: 'string'
            },
            code: {
              type: 'string'
            }
          }
        },
        settlementHistory: {
          type: 'object',
            properties: { 
                paymentID: {
                  type: "string"
                },              
                company_name: {
                  type: "string"
                },
                total_amount: {
                  type: "string"
                },
                GoInsure_amount: {
                  type: "string",
                },
                company_amount: {
                  type: "string",
                },
                settlement_date: {
                  type: "date"
                }
              }
            },
        Users: {
          type: 'object',
          properties: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/User'
            }
          }
        },
        Subscriptions: {
          type: 'object',
          properties: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/Subscription'
            }
          }
        },
        Products: {
          type: 'object',
          properties: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/Product'
            }
          }
        },
        Payments: {
          type: 'object',
          properties: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/Payment'
            }
          }
        },
        Services: {
          type: 'object',
          properties: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/Service'
            }
          }
        },
        Banks: {
          type: 'object',
          properties: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/Bank'
            }
          }
        },
        SettlementHistory: {
          type: 'object',
          properties: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/SettlementHistory'
            }
          }
        },
        Error: {
          type: 'object',
          properties: {
            message: {
              type: 'string'
            },
            internal_code: {
              type: 'string'
            }
          }
        }          
      }
};