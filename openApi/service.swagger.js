exports.getServices = {
    tags: ['Service'],
    summary: "Get All services",
    description: "Returns all services stored in the system",
    operationId: 'getServices',
    security: [
        {
            bearerAuth: []
        }
    ],
    responses: {
      '200': {
        description: 'Services were obtained',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Services'
            }
          }
        }
      },
      '400': {
        description: 'Missing parameters',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Error'
            },
            example: {
              message: 'id is missing',
              internal_code: 'missing_parameters'
            }
          }
        }
      }
    }
} 

exports.getService = {
  tags: [
    "Service"
  ],
  summary: "Get service by id",
  operationId: "getServiceById",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "The id of the Service that needs to be fetched. ",
      required: true,
      schema: {
        type: "string"
      }
    }
  ],
  responses: {
    200: {
      description: "successful operation",
      content: {
        "application/xml": {
          schema: {
            $ref: "#/components/schemas/Service"
          }
        },
        "application/json": {
          schema: {
            $ref: "#/components/schemas/Service"
          }
        }
      }
    },
    400: {
      description: "Invalid Service_id supplied",
      content: {}
    },
    404: {
      description: "ID not found",
      content: {}
    }
  }
}

exports.postService = {
    tags: [
        'Service'
      ],
      summary: "Create service",
      description: "Create a new service.",
      operationId: "createService",
      requestBody: {
        description: "Created service object",
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Service'
            }
          }
        },
        required: true
      },
      responses: {
        '200': {
          description: 'New service created'
        },
        '400': {
          description: 'Invalid parameters',
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/Error'
              },
              example: {
                message: 'User id 10, 20 already exist',
                internal_code: 'invalid_parameters'
              }
            }
          }
        }
    }
}

exports.exportService = {
  tags: [
      'Service'
    ],
    summary: "export Service",
    description: "export Service",
    operationId: "exportService",
    requestBody: {
      description: "example of string : firstname lastname",
      content: {
        'application/json': {
          schema: {
            properties:{
              fields: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      },
      422: {
        description: 'validation error'
      }
  }
};

exports.advancedServiceSearch = {
  tags: [
      'Service'
    ],
    summary: "Advanced Search service",
    description: "Search the Service Table.",
    operationId: "advancedServiceSearch",
    requestBody: {
      description: "found searched documents",
      content: {
        'application/json': {
          schema: {
            properties: {
              query: {
                type: "object",
                properties: {
                  field: {
                    type: "string"
                  }
                }
              },
              limit : {
                type: "number",
                default: 10
              },
              sort: {
                properties: {
                  field: {
                    type: "number",
                    enum: "-1, 1"
                  }
                }
              },
              select: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      '200': {
        description: 'database search is successful'
      },
      '400': {
        description: 'Invalid parameters',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Error'
            },
            example: {
              message: 'Service id 10, 20 already exist',
              internal_code: 'invalid_parameters'
            }
          }
        }
      }
  }
}

exports.searchService = {
    tags: [
        'Service'
      ],
      summary: "Search service",
      description: "Search the database.",
      operationId: "searchService",
      requestBody: {
        description: "found searched documents",
        content: {
          'application/json': {
            schema: {
              properties: {
                query: {
                  type: "object",
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: "-1, 1"
                    }
                  }
                },
                select: {
                  type: "string"
                }
              }
            }
          }
        },
        required: true
      },
      responses: {
        '200': {
          description: 'database search is successful'
        },
        '400': {
          description: 'Invalid parameters',
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/Error'
              },
              example: {
                message: 'Service id 10, 20 already exist',
                internal_code: 'invalid_parameters'
              }
            }
          }
        }
    }
}

exports.updateService = {
  tags: [
    "Service"
  ],
  summary: "Update service",
  description: "This can only be done by the logged in user.",
  operationId: "updateService",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of service that needs to be updated",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "Updated service's object",
    content: {
      "*/*": {
        schema: {
          properties: {    
            _id: {
              type: "string"
            },           
            productID: {
              type: "string"
            },
            parentID: {
              type: "string"
            },
            userID: {
              type: "string"
            },
            requirment: {
              type: "mixed",
            },
            summary: {
                type: 'string'
            },
            description: {
                type: 'string'
            },
            valueType: {
              type: "string",
              enum: ["1", "2"]
            },
            valueAmount: {
              type: "string",
            },
            duration: {
              type: "string",
              enum: ["1", "2"]
            }
          }
        }
      }
    },
    required: true
  },
  responses: {
    400: {
      description: "Invalid service's id supplied",
      content: {}
    },
    404: {
      description: "id not found",
      content: {}
    }
  },
  "x-codegen-request-body-name": "body"
}

exports.deleteService = {
  tags: [
    "Service"
  ],
  summary: "Delete service",
  description: "This can only be done by the logged in user.",
  operationId: "deleteService",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of service that needs to be deleted",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    400: {
      description: "Invalid service's id supplied",
      content: {}
    },
    404: {
      description: "id not found",
      content: {}
    }
  },
}