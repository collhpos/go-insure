exports.allSettlementHistory = {
    tags: ['SettlementHistory'],
    summary: "Get All SettlementHistory",
    description: "Returns all SettlementHistory from the system",
    operationId: 'allSettlementHistory',
    security: [
        {
            bearerAuth: []
        }
    ],
    responses: {
      '200': {
        description: 'success',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/SettlementHistory'
            }
          }
        }
      },
      '400': {
        description: 'Error'
      }
    }
} 

exports.getSettlementHistory = {
  tags: [
    "SettlementHistory"
  ],
  summary: "Get SettlementHistory by id",
  operationId: "getSettlementHistory",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "The id of the SettlementHistory that needs to be fetched. ",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    200: {
      description: "success",
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/SettlementHistory"
          }
        }
      }
    },
    400: {
      description: "Error"
    },
    404: {
      description: "No document found with that id"
    }
  }
}

exports.exportSettlementHistory = {
  tags: [
      'SettlementHistory'
    ],
    summary: "export SettlementHistory",
    description: "export SettlementHistory",
    operationId: "exportSettlementHistory",
    requestBody: {
      description: "example of string : firstname lastname",
      content: {
        'application/json': {
          schema: {
            properties:{
              fields: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      },
      422: {
        description: 'validation error'
      }
  }
};

exports.addSettlement = {
    tags: [
        'SettlementHistory'
      ],
      summary: "Create SettlementHistory",
      description: "Create a new SettlementHistory.",
      operationId: "addSettlement",
      requestBody: {
        description: "Created SettlementHistory object",
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/SettlementHistory'
            }
          }
        },
        required: true
      },
      responses: {
        '200': {
          description: 'success'
        },
        '400': {
          description: 'Error'
        }
    }
}

exports.searchSettlementHistory = {
    tags: [
        'SettlementHistory'
      ],
      summary: "Search SettlementHistory",
      description: "Search the database.",
      operationId: "searchSettlementHistory",
      requestBody: {
        description: "found searched documents",
        content: {
          'application/json': {
            schema: {
              properties: {
                query: {
                  type: "object",
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: "-1, 1"
                    }
                  }
                },
                select: {
                  type: "string"
                }
              }
            }
          }
        },
        required: true
      },
      responses: {
        '200': {
          description: 'success'
        },
        '400': {
          description: 'Error'
        }
    }
}

exports.updateSettlementHistory = {
  tags: [
    "SettlementHistory"
  ],
  summary: "Update SettlementHistory",
  description: "This can only be done by the logged in user.",
  operationId: "updateSettlementHistory",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of SettlementHistory that needs to be updated",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "updated",
    content: {
      "*/*": {
        schema: {
          properties: { 
            _id: {
              type: "MongoID"
            },
            name: {
              type: "string"
            }
          }
        }
      }
    },
    required: true
  },
  responses: {
    400: {
      description: "Error"
    },
    404: {
      description: "No document found with that id"
    }
  },
  "x-codegen-request-body-name": "body"
}

exports.deleteSettlementHistory = {
  tags: [
    "SettlementHistory"
  ],
  summary: "Delete SettlementHistory",
  description: "This can only be done by the logged in user.",
  operationId: "deleteSettlementHistory",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of SettlementHistory that needs to be deleted",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    400: {
      description: "Error"
    },
    404: {
      description: "No document found with that id"
    }
  },
}