exports.countCompanyEntities = {
    tags: [
      "CompanyEntities"
    ],
    summary: "Count company documents",
    operationId: "countCompanyEntities",
    security: [
        {
            bearerAuth: []
        }
    ],
    parameters: [
      {
        name: "id",
        in: "query",
        description: "The id of the company",
        required: true,
        schema: {
          type: "integer",
          format: "int64"
        }
      }
    ],
    responses: {
      200: {
        description: "success",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/Payment"
            }
          }
        }
      },
      400: {
        description: "error"
      }
    }
}