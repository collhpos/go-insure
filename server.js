const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cron = require('node-cron');
const subscriptionJob = require('./services/SubscriptionExpirationJob');

dotenv.config({
    path: './config.env'
});

// process.on('uncaughtException', err => {
//     console.log('UNCAUGHT EXCEPTION!!! shutting down...');
//     console.log(err.name, err.message);
//     process.exit(1);
// });

const app = require('./app');
//const IN_PROD = process.env.NODE_ENV === 'production'

///const database = IN_PROD ? process.env.DATABASE.replace('<PASSWORD>', process.env.DATABASE_PASSWORD) : process.env.MONGODB_URI ;
// let database = process.env.MONGODB_URI;
let database = process.env.DATABASE;

switch(process.env.NODE_ENV) {
    case 'production': 
         database = process.env.DATABASE;
         break;
    case 'staging':
        database = process.env.STAGING_URI;
        break;
    case 'qa':
         database = process.env.QA_MONGODB_URI;
         break;
    case 'development':
        database = process.env.MONGODB_URI;
        break;
    case 'local':
        database = process.env.DATABASEE;
        break;
    default:
        database = process.env.MONGODB_URI;    
}

// Connect the database
mongoose.connect(database, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
}).then(con => {
    console.log('DB connection Successful!');
});

// Start the server
const port = process.env.PORT || 3001;

app.listen(port, () => {  
    console.log(`Application is running on port ${port}`);
});

// Schedule tasks to be run on the server.
cron.schedule('0 0 * * *', function() {
    subscriptionJob.handleSubscriptionExpiryNotificationJob(5);
    subscriptionJob.handleSubscriptionExpiryNotificationJob(1);
    subscriptionJob.handleSubscriptionExpiryJob();
});

process.on('unhandledRejection', err => {
    console.log('UNHANDLED REJECTION!!!  shutting down ...');
    console.log(err.name, err.message);
    /* server.close(() => {
        process.exit(1);
    }); */
});